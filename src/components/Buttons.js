import React, { Component } from 'react';
import Button from './Button'

const Buttons = ({handleAdd,handleMinus,handleTimesTwo,handleReset}) => {

        return ( 
            <div 
            className="d-flex justify-content-around" 
            style={{
              margin:"0 200px"
            }}>
                <Button
                    color = {'btn btn-info'}
                    text = {'+1'}
                    handleOnClick = {handleAdd}
                />
                <Button
                    color = {'btn btn-warning'}
                    text = {'-1'}
                    handleOnClick = {handleMinus}
                />
                <Button
                    color = {'btn btn-success'}
                    text = {'Reset'}
                    handleOnClick = {handleReset}
                />
                <Button
                    color = {'btn btn-secondary'}
                    text = {'x2'}
                    handleOnClick = {handleTimesTwo}
                />
          </div>
         );
    
}
 
export default Buttons;


// Class
// import React, { Component } from 'react';
// import Button from './Button'

// class Buttons extends Component {
//     render() { 
//         return ( 
//             <div 
//             className="d-flex justify-content-around" 
//             style={{
//               margin:"0 200px"
//             }}>
//                 <Button
//                     color = {'btn btn-info'}
//                     text = {'+1'}
//                     handleOnClick = {this.props.handleAdd}
//                 />
//                 <Button
//                     color = {'btn btn-warning'}
//                     text = {'-1'}
//                     handleOnClick = {this.props.handleMinus}
//                 />
//                 <Button
//                     color = {'btn btn-success'}
//                     text = {'Reset'}
//                     handleOnClick = {this.props.handleReset}
//                 />
//                 <Button
//                     color = {'btn btn-secondary'}
//                     text = {'x2'}
//                     handleOnClick = {this.props.handleTimesTwo}
//                 />
//           </div>
//          );
//     }
// }
 
// export default Buttons;