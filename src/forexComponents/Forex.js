import React, { Component } from 'react';
import ForexDropdown from './ForexDropdown'
import ForexInput from './ForexInput'
import Rates from './Rates'
import {Button} from 'reactstrap'

class Forex extends Component {

    state ={
        amount: 0,
        baseCurrency: null,
        targetCurrency:null,
        convertedAmount:0,
        convertedCode:null,
        rates:{}
    }

    handleAmount = e =>{
        this.setState({
            amount:e.target.value
        })
    }

    handleBaseCurrency = currency =>{
        this.setState({
            baseCurrency: currency
        })
        const code = currency.code;
        // console.log(currency)
            fetch('https://api.exchangeratesapi.io/latest?base=' + code)
            .then(res => res.json())
            .then(res=>{
            this.setState({ rates: res.rates  });
           
            
        })
       
       
    }

    handleTargetCurrency = currency =>{
        this.setState({
            targetCurrency: currency
        })
    }

 

    handleConvert = () => {

        if(this.state.amount <= 0){
            alert('Invalid Input')
        }else if(this.state.baseCurrency === null && this.state.targetCurrency === null ){
            alert('Please Choose Currency')
        }else if(this.state.baseCurrency === null || this.state.targetCurrency === null){
            alert('Please Choose Currency')
        }else {
            const code = this.state.baseCurrency.code;

            fetch('https://api.exchangeratesapi.io/latest?base=' + code)
            .then(res => res.json())
            .then(res=>{
            
            const targetCode = this.state.targetCurrency.code

            const rate = res.rates[targetCode]

            // console.log(rate)
            this.setState({convertedAmount:this.state.amount*rate})
            this.setState({convertedCode:targetCode})
        })
        }

    }

    render() { 
        return ( 
            <div style={{width: "70%"}}>
                <h1 className='text-center my-5'>Forex Calculator</h1>
                <div className="d-flex justify-content-around"
                    style={{margin:'0 200px'}}
                >
                    <ForexDropdown
                        label ={'Base Currrency'}
                        onClick={this.handleBaseCurrency}
                        currency = {this.state.baseCurrency}
                    
                     />
                     <ForexDropdown
                        label ={'Target Currrency'}
                        onClick={this.handleTargetCurrency}
                        currency ={this.state.targetCurrency}
                     />
                </div>
                <div className="d-flex justify-content-around">
                    <ForexInput
                        label = {'Amount'}
                        placeholder ={'Amout to Convert'}
                        onChange = {this.handleAmount}

                     />
                     <Button
                     color = "info"
                     onClick = {this.handleConvert}
                     >
                     Convert
                     </Button>
                     <div>
                         <h1 className = 'text-center'>{this.state.convertedAmount} {this.state.convertedCode ?this.state.convertedCode :""} </h1>
                     </div>
                </div>
                <div style={{maxHeight: "40vh", overflowY: "auto"}}>
                    <Rates 
                        rates = {this.state.rates}
                     />
                </div>
                
            </div>
         );
    }
}
 
export default Forex;